#!/bin/bash
#PJM -L "rscgrp=ito-ss-dbg"
#PJM -L "rscunit=ito-a"
#PJM -L "vnode=1"
#PJM -L "vnode-core=36"
#PJM -L "elapse=1:00:00"
#PJM --mpi "proc=36"
#PJM -S

# Share settings
source ITO/runShare.sh
# Run command
restore0Dir
blockMesh &> log.blockMesh.$jobid
setFields &> log.setFields.$jobid
decomposePar $options  &> log.decomposePar.$jobid

# crownInterFoam
Test case to simulate milk crown for interFoam  
The execution commands are:  
$ cp -r 0.orig 0  
$ blockMesh  
$ setFields  
$ decomposePar (if necessary)  
$ interFoam (mpirun -np * interFoam -parallel)  
$ reconstructPar (if necessary)  

# crownInterIsoFoam
Test case to simulate milk crown for interIsoFoam  
The execution commands are:  
$ cp -r 0.orig 0  
$ blockMesh  
$ setFields  
$ decomposePar (if necessary)  
$ interIsoFoam (mpirun -np * interIsoFoam -parallel)  
$ reconstructPar (if necessary)  

#### Note
In both cases, the total number of numerical grid cells is large.
Please change the grid resolution through system/blockMeshDict if the execution time becomes too long.

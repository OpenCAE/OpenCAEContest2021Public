#!/bin/bash
#PJM -L "rscgrp=ito-s-dbg"
#PJM -L "rscunit=ito-a"
#PJM -L "vnode=2"
#PJM -L "vnode-core=36"
#PJM -L "elapse=1:00:00"
#PJM --mpi "proc=72"
#PJM -S

# Share settings
source ITO/runShare.sh
# Run command
$mpirun -np $PJM_MPI_PROC $application $options -parallel >& log.$application.$jobid

#!/bin/bash
#PJM -g gz00
#PJM -L rscgrp=debug
#PJM -L elapse="0:30:00"
#PJM -L node=1
#PJM --mpi proc=1
#PJM -j
#PJM -S

# Share settings
source OBCX/runShare.sh
# Run command
restore0Dir
blockMesh &> log.blockMesh.$jobid
setFields &> log.setFields.$jobid
decomposePar $options  &> log.decomposePar.$jobid

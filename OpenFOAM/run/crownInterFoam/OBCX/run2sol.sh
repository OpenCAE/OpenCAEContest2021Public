#!/bin/bash
#PJM -g gz00
#PJM -L rscgrp=short
#PJM -L elapse="2:00:00"
#PJM -L node=1
#PJM --mpi proc=48
#PJM -j
#PJM -S

# Share settings
source OBCX/runShare.sh
# Run command
$mpirun -np $PJM_MPI_PROC $application $options -parallel >& log.$application.$jobid
